#!/bin/bash

set -ue

cd $(dirname $(readlink -f $0))

# Order matters here: the first distributions listed have priority
SUITES="main contrib non-free"
DEBIANDISTS="testing sid stable experimental"
UBUNTUDISTS="precise trusty wily xenial"

progress ()
{
	if [ -t 2 ]
	then
		echo "$@" >&2
	fi
}

curlcat ()
{
	tag="$1"
	url="$2"
	if curl -I -s -f "$url" > /dev/null
	then
		#echo "Package: $tag"
		#echo "Description: $url"
		#echo
		curl -s -f "$url" | gzip -cd
	fi
}

debian_pkgs ()
{
	dist=$1
	for d in $dist $dist-backports $dist-updates
	do
		for suite in main contrib non-free
		do
			for arch in all amd64 i386 armel armhf ia64 kfreebsd-amd64 kfreebsd-i386 mipsel mips powerpc s390x sparc
			do
				curlcat "debian-$d-$suite-$arch" "http://ftp.debian.org/debian/dists/$d/$suite/binary-$arch/Packages.gz"
			done
		done
	done
}

debian_srcs ()
{
	dist=$1
	for d in $dist $dist-backports $dist-updates
	do
		for suite in main contrib non-free
		do
			curlcat "debian-$d-$suite" "http://ftp.debian.org/debian/dists/$d/$suite/source/Sources.gz"
		done
	done
}

ubuntu_pkgs ()
{
	dist=$1
	for d in $dist $dist-backports $dist-proposed $dist-security $dist-updates
	do
		for suite in main multiverse restricted universe
		do
			for arch in amd64 i386
			do
				curlcat "ubuntu-$d-$suite-$arch" "http://ftp.ubuntu.com/ubuntu/dists/$d/$suite/binary-$arch/Packages.gz"
			done
		done
	done

}

ubuntu_srcs ()
{
	dist=$1
	for d in $dist $dist-backports $dist-proposed $dist-security $dist-updates
	do
		for suite in main multiverse restricted universe
		do
			curlcat "ubuntu-$d-$suite" "http://ftp.ubuntu.com/ubuntu/dists/$d/$suite/source/Sources.gz"
		done
	done

}

rm -f *merged*.gz

progress "Processing Debian packages..."
(
	for dist in $DEBIANDISTS
	do
		progress "Merging debian $dist..."
		debian_pkgs $dist | ./mp $dist | tee merged-$dist
		gzip merged-$dist
	done
) | ./mp "" | gzip > debian-merged.gz

progress "Processing Ubuntu packages..."
(
	for dist in $UBUNTUDISTS
	do
		progress "Merging ubuntu $dist..."
		ubuntu_pkgs $dist | ./mp $dist | tee merged-$dist
		gzip merged-$dist
	done
) | ./mp "" | gzip > ubuntu-merged.gz

progress "Merging all packages..."
zcat debian-merged.gz ubuntu-merged.gz | ./mp "" | gzip > all-merged.gz

progress "Processing Debian sources..."
(
	for dist in $DEBIANDISTS
	do
		progress "Merging debian $dist..."
		debian_srcs $dist | ./mp -s $dist | tee merged-sources-$dist
		gzip merged-sources-$dist
	done
) | ./mp -s "" | gzip > debian-merged-sources.gz

progress "Processing Ubuntu sources..."
(
	for dist in $UBUNTUDISTS
	do
		progress "Merging ubuntu $dist..."
		ubuntu_srcs $dist | ./mp -s $dist | tee merged-sources-$dist
		gzip merged-sources-$dist
	done
) | ./mp -s "" | gzip > ubuntu-merged-sources.gz

progress "Merging all sources..."
zcat debian-merged-sources.gz ubuntu-merged-sources.gz | ./mp -s "" | gzip > all-merged-sources.gz

mv *merged* /srv/debtags.debian.org/htdocs/mergepackages/
