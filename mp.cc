#include <map>
#include <set>
#include <string>
#include <algorithm>
#include <iterator>
#include <iostream>

#include <apt-pkg/tagfile.h>
#include "regexp.h"

using namespace std;

ostream& operator<<(ostream& out, const set<string>& items)
{
    bool first = true;
    for (const auto& i: items)
    {
        if (first)
            first = false;
        else
            out << ", ";
        out << i;
    }
    return out;
}

struct Parser
{
    Splitter split_commas{"[ \t\n]*,[ \t\n]*", REG_EXTENDED};
    Splitter split_deps{"[ \t\n]*[,|][ \t\n]*", REG_EXTENDED};

    void merge_list(set<string>& tgt, const std::string& src)
    {
        std::copy(split_commas.begin(src), split_commas.end(), inserter(tgt, tgt.begin()));
    }

    void merge_deplist(set<string>& tgt, const std::string& src)
    {
        for (auto i = split_deps.begin(src); i != split_deps.end(); ++i)
        {
            size_t pos = i->find(" ");
            if (pos == string::npos)
                tgt.insert(*i);
            else
                tgt.insert(i->substr(0, pos));
        }
    }

    void max_number(size_t& tgt, const std::string& src)
    {
        if (src.empty()) return;
        unsigned long int srcval = stoul(src);
        if (srcval > tgt)
            tgt = srcval;
    }
} parser;

struct Pkg
{
    string name;
    string version;
    set<string> distros;
    set<string> depends;
    set<string> predepends;
    set<string> recommends;
    set<string> suggests;
    set<string> enhances;
    set<string> provides;
    set<string> conflicts;
    set<string> replaces;
    set<string> arches;
    string source;
    string tags;
    string section;
    string description;
    size_t size = 0;
    size_t inst_size = 0;

    void scan_first(const std::string& dist, const pkgTagSection& ts)
    {
        // Mark it as seen in this distro
        if (!dist.empty()) distros.insert(dist);
        tags = ts.FindS("Tag");
        section = ts.FindS("Section");
        description = ts.FindS("Description");
        source = ts.FindS("Source");
        size_t pos = source.find(" ");
        if (pos != string::npos)
            source = source.substr(0, pos);
    }

    void scan(const pkgTagSection& ts)
    {
        // Merge fields
        parser.merge_list(distros, ts.FindS("Distribution"));
        parser.merge_list(arches, ts.FindS("Architecture"));

        parser.merge_deplist(depends, ts.FindS("Depends"));
        parser.merge_deplist(predepends, ts.FindS("Pre-Depends"));
        parser.merge_deplist(recommends, ts.FindS("Recommends"));
        parser.merge_deplist(suggests, ts.FindS("Suggests"));
        parser.merge_deplist(enhances, ts.FindS("Enhances"));
        parser.merge_deplist(provides, ts.FindS("Provides"));
        parser.merge_deplist(conflicts, ts.FindS("Conflicts"));
        parser.merge_deplist(replaces, ts.FindS("Replaces"));

        parser.max_number(size, ts.FindS("Size"));
        parser.max_number(inst_size, ts.FindS("Installed-Size"));
    }

    void print(ostream& out) const
    {
        out << "Package: " << name << endl;
        out << "Version: " << version << endl;
        out << "Distribution: " << distros << endl;
        if (!depends.empty())
            out << "Depends: " << depends << endl;
        if (!predepends.empty())
            out << "Pre-Depends: " << predepends << endl;
        if (!recommends.empty())
            out << "Recommends: " << recommends << endl;
        if (!suggests.empty())
            out << "Suggests: " << suggests << endl;
        if (!enhances.empty())
            out << "Enhances: " << enhances << endl;
        if (!provides.empty())
            out << "Provides: " << provides << endl;
        if (!conflicts.empty())
            out << "Conflicts: " << conflicts << endl;
        if (!replaces.empty())
            out << "Replaces: " << replaces << endl;
        out << "Architecture: " << arches << endl;
        if (!source.empty())
            out << "Source: " << source << endl;
        if (!tags.empty())
            out << "Tag: " << tags << endl;
        out << "Section: " << section << endl;
        out << "Description: " << description << endl;
        out << "Size: " << size << endl;
        out << "Installed-Size: " << inst_size << endl;
        out << endl;
    }
};

struct Src
{
    string name;
    string version;
    string maintainer;
    string uploaders;
    set<string> builddepends;
    set<string> builddependsindep;

    void scan_first(const std::string& dist, const pkgTagSection& ts)
    {
        maintainer = ts.FindS("Maintainer");
        uploaders = ts.FindS("Uploaders");
    }

    void scan(const pkgTagSection& ts)
    {
        parser.merge_deplist(builddepends, ts.FindS("Build-Depends"));
        parser.merge_deplist(builddependsindep, ts.FindS("Build-Depends-Indep"));
    }

    void print(ostream& out) const
    {
        out << "Package: " << name << endl;
        out << "Version: " << version << endl;
        if (!maintainer.empty())
            out << "Maintainer: " << maintainer << endl;
        if (!uploaders.empty())
            out << "Uploaders: " << uploaders << endl;
        if (!builddepends.empty())
            out << "Build-Depends: " << builddepends << endl;
        if (!builddependsindep.empty())
            out << "Build-Depends-Indep: " << builddependsindep << endl;
        out << endl;
    }
};

template<typename PKG>
struct DB
{
    map<string, PKG> pkgs;

    void scan_pkg(const std::string& dist, const pkgTagSection& ts)
    {
        string name = ts.FindS("Package");
        string version = ts.FindS("Version");
        // Get the package record
        auto i = pkgs.find(name);
        if (i == pkgs.end())
        {
            // If it's a new record, initialise non-merged fields
            auto i = pkgs.insert(make_pair(name, PKG()));
            PKG& pkg = i.first->second;
            pkg.name = name;
            pkg.version = version;
            pkg.scan_first(dist, ts);
            pkg.scan(ts);
        } else if (i->second.version == version)
            i->second.scan(ts);
    }

    void scan_file(const std::string& dist, const std::string& fname)
    {
        FileFd infd(fname, FileFd::ReadOnlyGzip);
        pkgTagFile tf(&infd);
        pkgTagSection ts;
        while (tf.Step(ts))
        {
            scan_pkg(dist, ts);
        }
    }

    void scan_stdin(const std::string& dist)
    {
        FileFd infd(0);
        pkgTagFile tf(&infd);
        pkgTagSection ts;
        while (tf.Step(ts))
        {
            scan_pkg(dist, ts);
        }
    }

    void run(const std::string& dist, const vector<string>& inputs)
    {
        if (inputs.empty())
            scan_stdin(dist);
        else
            for (const auto& i: inputs)
                scan_file(dist, i);
        print(cout);
    }

    void print(ostream& out) const
    {
        for (const auto i: pkgs)
            i.second.print(out);
    }
};

int main(int argc, const char* argv[])
{
    // Parse command line
    if (argc < 2)
    {
        cerr << "Usage: " << argv[0] << " [-s] distname [pkgfile1.gz pkgfile2.gz ...]" << endl;
        return 1;
    }

    int argidx = 1;
    bool do_src = false;

    if (string(argv[argidx]) == "-s")
    {
        do_src = true;
        ++argidx;
    }

    string dist = argv[argidx];
    ++argidx;

    vector<string> inputs;
    for (int i = argidx; i < argc; ++i)
        inputs.push_back(argv[i]);


    if (do_src)
    {
        DB<Src> db;
        db.run(dist, inputs);
    } else {
        DB<Pkg> db;
        db.run(dist, inputs);
    }

    return 0;
}
